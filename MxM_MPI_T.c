#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

double Inicio, Fin;



int main(int argc, char *argv[])
{
    int miID, numProcs;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &miID);

    MPI_Status status;

    time_t t;
    double **ptr1, **ptr2, **ptr3;
    int N; //col1, row2, col2;
    srand ((unsigned) time (&t));
    int i, j, k, m;
    //printf ("\nEnter the value of N : ");
    //scanf ("%d", &N);
    N = atoi(argv[1]);
    //printf("%d\n",N );
    ptr1 = (double **) malloc (sizeof (double *) * N);
    ptr2 = (double **) malloc (sizeof (double *) * N);
    ptr3 = (double **) malloc (sizeof (double *) * N);

    for (i = 0; i < N; i++)
        ptr1[i] = (double *) malloc (sizeof (double) * N);
    for (i = 0; i < N; i++)
        ptr2[i] = (double *) malloc (sizeof (double) * N);
    for (i = 0; i < N; i++)
        ptr3[i] = (double *) malloc (sizeof (double) * N);

   
    
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            ptr1[i][j] = rand ();
        }
    }

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            ptr2[i][j] = rand ();
        }
    }
    Inicio = MPI_Wtime();        
    for (m = 0; m < N; ++m)
    {
        for (k = 0; k < N; ++k)
        {

            if(miID == 0)
            {                
                MPI_Send(ptr1[m],N,MPI_DOUBLE,1,2,MPI_COMM_WORLD);
                MPI_Send(ptr2[k],N,MPI_DOUBLE,1,2,MPI_COMM_WORLD);

                MPI_Recv(&ptr3[m][k],N,MPI_DOUBLE,1,2,MPI_COMM_WORLD, &status);
                if(m == N-1 && k ==N-1){
                    Fin = MPI_Wtime();
                    printf ("%f,", Fin-Inicio);
    }
             }   

            if(miID>0)
            {
                
                double *varDestino1 = (double *) malloc (sizeof (double) * N);
                double *varDestino2 = (double *) malloc (sizeof (double) * N);
                double varDestino3 = 0;
            
                MPI_Recv(varDestino1,N,MPI_DOUBLE,0,2,MPI_COMM_WORLD, &status);
                MPI_Recv(varDestino2,N,MPI_DOUBLE,0,2,MPI_COMM_WORLD, &status);

                for (i = 0; i < N; i++)
                    { 
                        varDestino3 = varDestino3 + varDestino1[i] * varDestino2[i];
                    }


                MPI_Send(&varDestino3,1,MPI_DOUBLE,0,2,MPI_COMM_WORLD);
                free(varDestino1);
                free(varDestino2);
            }
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);            
    MPI_Finalize();
    


    

    /* Printing the contents of third matrix. */
/*    printf ("\n\nMatrix 1:");
    for (i = 0; i < N; i++) {
        printf ("\n\t");
        for (j = 0; j < N; j++)
            printf ("%4f \t", ptr1[i][j]);
    }

    printf ("\n\nMatrix 2:");
    for (i = 0; i < N; i++) {
        printf ("\n\t");
        for (j = 0; j < N; j++)
            printf ("%4f \t", ptr2[i][j]);
    }

    printf ("\n\nFinal Matrix :");
    for (i = 0; i < N; i++) {
        printf ("\n\t");
        for (j = 0; j < N; j++)
            printf ("%9f \t", ptr3[i][j]);
    }*/

    for(i = 0; i < N; i++){
        free(ptr1[i]);
        free(ptr2[i]);
        free(ptr3[i]);

    }

    free(ptr1);
    free(ptr2);
    free(ptr3);

    //printf ("\n");
    //printf ("tiempo : %4f segundos \t", Fin-Inicio);
    return (0);
}